FROM node:16-alpine3.17

ENV TERM=xterm
ENV TZ=Asia/Bangkok
RUN apk update
RUN apk add --no-cache bash
RUN apk add --no-cache lcms2-dev
RUN apk add --no-cache libpng-dev
RUN apk add --no-cache gcc
RUN apk add --no-cache g++
RUN apk add --no-cache make
RUN apk add --no-cache automake
RUN apk add --no-cache autoconf
RUN apk add --no-cache ffmpeg

WORKDIR /var/task
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 8080
CMD [ "node", "server.js" ]