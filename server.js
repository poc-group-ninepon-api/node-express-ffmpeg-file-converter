require('dotenv').config()
const express = require('express');
const fs = require('fs');
const ffmpeg = require("fluent-ffmpeg");
const { exec, spawn, execSync } = require('node:child_process');
var AWS = require('aws-sdk');
const formidable = require('formidable');
let app = express();

const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_REGION,
})

app.post('/', function(req, res){

    var form = new formidable.IncomingForm();
    form.parse(req);

    let assetName = null
    let convertTo = null
    let s3Key = null
    let originalFileType = null
    let fileUpload = null
    let fileUploadName = null

    form.on('field', (fieldName, fieldValue) => {
        if(fieldName == 'assetName'){
            assetName = fieldValue
        }
        if(fieldName == 'convertTo'){
            convertTo = fieldValue
        }
        if(fieldName == 's3Key'){
            s3Key = fieldValue
        }
        if(fieldName == 'originalFileType'){
            originalFileType = fieldValue
        }
    });

    form.on('file', function (name, file){
        fileUploadName = name
        fileUpload = file
    });

    form.on('end', async () => {
        console.log(`formidable end`);
        console.log(assetName, convertTo, s3Key, originalFileType);

        const splitFileNmae = assetName.split('.')
        const newNameforFormat = `${splitFileNmae[0]}.${convertTo}`
        const oldFileNameWithPathInTmp = `/tmp/${s3Key}${assetName}`
        const newFileNameWithPathInTmp = `/tmp/${s3Key}${newNameforFormat}`

        // const originalFileName = fileUpload.originalFilename
        const tmpOldFile = fileUpload.filepath
        // const tmpNewFile = `/tmp/pon-image/${originalFileName}`

        await execSync(`mkdir -p /tmp/${s3Key}`)
        console.log(`create tmp`);
        const downloadToLocal = new Promise((resolve,reject) => {
            var rawData = fs.readFileSync(tmpOldFile)
            fs.writeFile(oldFileNameWithPathInTmp, rawData, (err) => {
                if(err) { console.log(`fs.writeFile err`, err); reject() } else { console.log(`fs.writeFile ok`); resolve() }
            })
        });

        await downloadToLocal
            .then(async ()=>{
                const ConvertAssetAndDelete = new Promise((resolve,reject)=>{
                    if(originalFileType !== "mp4"){
                        ffmpeg()
                        .input(oldFileNameWithPathInTmp)
                        .inputOptions("-loop 1")
                        .videoCodec("libvpx-vp9")
                        .outputOptions("-t 5")
                        .output(newFileNameWithPathInTmp)
                        .on("start", (commandLine) => {
                            console.log("Spawned Ffmpeg with command: " + commandLine);
                        })
                        .on("error", (err) => {
                            console.log("An error occurred: " + err.message);
                            reject()
                        })
                        .on("end", () => {
                            console.log("Processing finished !");
                            resolve()
                        })
                        .run();
                    }else{
                        ffmpeg()
                        .input(oldFileNameWithPathInTmp)
                        .videoCodec("libvpx-vp9")
                        .audioCodec("opus")
                        .output(newFileNameWithPathInTmp)
                        .on("start", (commandLine) => {
                            console.log("Spawned Ffmpeg with command: " + commandLine);
                        })
                        .on("error", (err) => {
                            console.log("An error occurred: " + err.message);
                            reject()
                        })
                        .on("end", async () => {
                            console.log("Processing finished !");
                            resolve()
                        })
                        .run();
                    }
                })
        
                await ConvertAssetAndDelete
                    .then(async ()=>{
                        //UPLOAD to S3
                        const uploadS3 = new Promise( async (resolve, reject) => {
                            try{
                                const fileContent = fs.readFileSync(newFileNameWithPathInTmp)
                                const params = {
                                    Body: fileContent,
                                    Bucket: `${process.env.S3_BUCKET}`,
                                    Key: `${s3Key}${newNameforFormat}`,
                                    ACL: "public-read",
                                }
                                const res = await s3.upload(params).promise()
                                resolve(res)
                            }catch(err){
                                reject(err)
                            }
                        });

                        await uploadS3
                            .then( async (resUploadS3)=>{
                                //delete asset before convert and after convert
                                await execSync(`rm ${newFileNameWithPathInTmp} && rm ${oldFileNameWithPathInTmp}`)

                                res.status(200).json( resUploadS3 )
                            })
                            .catch((err)=>{
                                res.status(500).json({ message: `error upload S3: ${err}` })
                            })
                    })
                    .catch(()=>{
                        res.status(500).json({ message: `error ffmpeg convert file` })
                    })
            })
            .catch(()=>{
                res.status(500).json({ message: `error download to local` })
            })
    });
});

app.listen(8080);